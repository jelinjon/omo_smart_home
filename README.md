# OMO term project repository

This repository contains code for managing various aspects of a smart house, such as controlling lights, temperature, security systems, and other automated functions, as well as its residents. It helps create a seamless and convenient living environment.