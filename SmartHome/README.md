# Smart Home Simulation Project

## Description
This project simulates the functionality of a smart home, allowing users to interact with various smart devices such as light bulbs, thermostats, etc.

## Table of Contents
- [Usage](#Usage)
- [Classes](#Classes)
  - [Person Class](#person-class)
    - [Attributes](#attributes)
    - [Constructor and Builder](#constructor-and-builder)
      - [Example usage](#example-usage)
    - [Methods](#methods)
        - [`setState(HealthState healthState)`](#setstatehealthstate-healthstate)
        - [`isHealthy(): boolean`](#ishealthy-boolean)
        - [`nextHealthState()`](#nexthealthstate)
        - [`updatePerson(int hour)`](#updatepersonint-hour)
        - [`calculateExpenses(): MonetaryAmount`](#calculateexpenses-monetaryamount)
  - [Home Class](#home-class)
      - [Overview](#overview)
      - [Attributes](#attributes-1)
      - [Constructors](#constructors)
      - [Methods](#methods-1)
      - [Usage Example](#usage-example)
      - [Note](#note)
      - [Dependencies](#dependencies)
  - [Floor Class](#floor-class)
      - [Overview](#overview-1)
      - [Attributes](#attributes-2)
      - [Constructors]()
      - [Methods]()
  - [Window Class](#window-class)
      - [Overview](#overview-1)
      - [Attributes](#attributes-2)
      - [Constructors]()
      - [Methods]()
  - [Appliance Class](#appliance-class)
      - [Overview]()
      - [Attributes]()



## Usage
1. Open simulation package and Simulation class
2. Configure the amount of hours the simulation should run for in method startSimulation parameter
3. Choose house config or use House Builder nested class to customize a house build
4. Configure logger level if desired, default is OFF
5. Run the main method

## Classes
### Person Class

The `Person` class models an individual in the smart home simulation. Each person has attributes related to their daily activities, health state, and schedule. The class includes functionality for updating the person's state and performing various actions based on the simulation time.
The Persons schedule is created randomly at midnight from actions appropriate to his health state and day of the week. Actions have a set length in hours, after which they change for the next in schedule, Person also moves from Room to Room dependent on current Activity.

#### Attributes

- `isAsleep`: Indicates whether the person is currently asleep.
- `wakeUpTime`: The time at which the person wakes up (default: 6 AM).
- `goToSleepTime`: The time at which the person goes to sleep (default: 10 PM).
- `name`: The name of the person.
- `isAtHome`: Indicates whether the person is currently at home.
- `currentlyDoing`: The activity the person is currently engaged in.
- `dailySchedule`: The list of activities planned for the day.
- `healthState`: The current health state of the person.
- `strategy`: The strategy used to determine the person's daily schedule.

#### Constructor and Builder

The `Person` class provides a default constructor and a builder pattern for creating instances with specific attributes. The builder allows you to set the person's name and customize wake-up and sleep times.

##### Example usage

```
Person person = new Person.PersonBuilder()
.addName("John Doe")
.addWakeUpHour(7)
.addGoToSleepHour(23)
.build();
```

#### Methods

##### `setState(HealthState healthState)`

Sets the health state of the person.

##### `isHealthy(): boolean`

Returns `true` if the person is currently in a healthy state.

##### `nextHealthState()`

Changes the person's health state to the next state in the progression.

##### `updatePerson(int hour)`

Updates the person's attributes and performs actions based on the simulation time (hour).

##### `calculateExpenses(): MonetaryAmount`

Calculates the daily expenses of the person (placeholder logic).

### Home Class

#### Overview

The `Home` class represents a virtual house and contains information about the house's floors, people residing in it, sport equipment owned, owned pet animals, and methods to update its state. This class is part of the "omo.home.model" package.

#### Attributes

- **hourOfTheDay (int):** Represents the current hour of the day.
- **floors (List<Floor>):** A list of floor objects within the house.
- **people (List<Person>):** A list of people residing in the house.

#### Constructors

- **Home():** Default constructor that initializes the list of people with four default Person objects.

#### Methods

- **updateState():** Updates the state of the house by calling the `updatePerson` method for each person in the list. It also increments the `hourOfTheDay` and resets it to 0 if it exceeds 23.

#### Usage Example

```
// Create a new Home object
Home myHome = new Home();

// Update the state of the house
myHome.updateState();
```

#### Note
The updateState method prints information about the execution and the current hour of the day.

#### Dependencies
The class depends on the "omo.home.model.people.Person" class for representing individuals in the house.


### Floor Class

#### Overview
The `Floor` class represents a floor inside the virtual house and contains information about the floor's number, being 0 for ground floor, the rooms on the floor and an internal index counter that iterates with every floor created to keep the number of floors consistent throughout the house. This class is part of the "omo.home.model" package.

### Furniture Class

#### Overview
The `Furniture` class represents a piece furniture inside the virtual house and contains information about its location. 
This class is part of the "omo.home.model" package.


### Window Class

#### Overview
The `Window` class keeps an index, location and an instance of WindowBlinds, which is an Observer dependent on Clock observable and opens, closes and draws in relation to hour of day.

### Appliance Class

#### Overview
The `Appliance` class is an abstract class for various types of appliances located inside the Home. Each Appliance subclass has its own update method and behaves differently in relation to time of day.

#### Attributes
````
    private Room room;

    //in watts per hour
    protected int powerConsumptionActive;
    protected int powerConsumptionInactive;
    protected int powerConsumptionIdle;

    protected State state;
````
- Room - location
- Power consumption for appropriate State, in Watts per hour, as consumption is read hourly with updates. Approaches real life values of W/h consumption values for the appropriate type of appliance.
- State - ACTIVE, IDLE, INACTIVE - using nested enum within Appliance, represents real life states