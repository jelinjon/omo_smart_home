package omo.home.model.Animal;

import omo.home.model.Home;
import omo.home.model.people.state.HealthContext;
import omo.home.model.people.state.Healthy;
import omo.home.model.people.state.HealthState;
import omo.home.model.rooms.Outside;
import omo.home.model.rooms.Room;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class AnimalCat implements HealthContext {
    // Attributes
    private String name = "";
    private Activity currentlyDoing;
    private Activity outCurrentlyDoing;
    private List<Activity> dailySchedule;
    private HealthState healthState = new Healthy(this);
    //    private DayStrategy strategy = new WeekStrategy();
    private Personality personality;
    private Home home;
    private Room location;
    private int timeToLive;
    private boolean isAlive = true;

    // Builder
    public AnimalCat(AnimalCatBuilder builder) {
        this.name = builder.name;
        this.home = builder.home;
        this.personality = builder.personality;
        this.timeToLive = builder.timeToLive;
        this.location = builder.location;

        dailySchedule = new ArrayList<>();

        addCleanItselfActivities();
        addEatActivities();
        addPersonalityBasedActivities();
        addSleepActivities();
        Collections.shuffle(dailySchedule);

    }

    public static class AnimalCatBuilder {
        private String name;
        private Home home;
        private Room location;
        private Personality personality;
        private int timeToLive;

        public AnimalCatBuilder addName(String name) {
            this.name = name;
            return this;
        }

        public AnimalCatBuilder addHome(Home home) {
            this.home = home;
            return this;
        }

        public AnimalCatBuilder addPersonality(Personality personality) {
            this.personality = personality;
            return this;
        }
        public AnimalCatBuilder addTimeToLive(int timeToLive) {
            this.timeToLive = timeToLive;
            return this;
        }

        //  CAT TIME TO LIVE SETTER
        public AnimalCatBuilder setTimeToLive() {
            // Define constants
            int hoursPerDay = 24;
            int daysPerYear = 365;
            int maxNumberOfYears = 30;

            // Calculate total number of hours within a random number of years
            int numberOfYears = new Random().nextInt(maxNumberOfYears) + 1;
            this.timeToLive = hoursPerDay * daysPerYear * numberOfYears;
//            this.timeToLive = 5;
            return this;
        }

        public AnimalCat build() {
            return new AnimalCat(this);
        }
    }

    // setup based on personality
    private void addPersonalityBasedActivities() {
        int frequencyE;
        switch (personality) {
            case FRIENDLY:
                frequencyE = new Random().nextInt(4) + 2;
                addActivityBasedOnPersonality(Activity.HUNT, 2);
                addActivityBasedOnPersonality(Activity.PLAY, 5);
                addActivityBasedOnPersonality(Activity.CUDDLE, 10);
                addActivityBasedOnPersonality(Activity.BITE, 0);
                break;
            case AGGRESSIVE:
                frequencyE = new Random().nextInt(2) + 1;
                addActivityBasedOnPersonality(Activity.HUNT, 2);
                addActivityBasedOnPersonality(Activity.PLAY, 1);
                addActivityBasedOnPersonality(Activity.CUDDLE, 1);
                addActivityBasedOnPersonality(Activity.BITE, 1);
                break;
            case LAZY:
                frequencyE = new Random().nextInt(3) + 3;
                addActivityBasedOnPersonality(Activity.HUNT, 0);
                addActivityBasedOnPersonality(Activity.PLAY, 1);
                addActivityBasedOnPersonality(Activity.CUDDLE, 5);
                addActivityBasedOnPersonality(Activity.BITE, 0);
                break;
            default:
                frequencyE = 0;
                break;
        }
        addActivityBasedOnPersonality(Activity.EAT, frequencyE);
        fillUpSleepActivities();
    }

    // Fill up daily schedule with sleep activities
    private void fillUpSleepActivities() {
        int sleepFrequency = 24 - dailySchedule.size(); // Calculate remaining hours
        for (int i = 0; i < sleepFrequency; i++) {
            dailySchedule.add(Activity.SLEEP);
        }
    }


    // Activities
    private void addCleanItselfActivities() {
        int cleanItselfFrequency = new Random().nextInt(6) + 1;
        for (int i = 0; i < cleanItselfFrequency; i++) {
            dailySchedule.add(Activity.CLEAN_ITSELF);
        }
    }

    private void addEatActivities() {
        int eatFrequency = new Random().nextInt(4) + 2;
        for (int i = 0; i < eatFrequency; i++) {
            dailySchedule.add(Activity.EAT);
        }
    }

    private void addSleepActivities() {
        int sleepFrequency = new Random().nextInt(4) + 1;
        for (int i = 0; i < sleepFrequency; i++) {
            dailySchedule.add(Activity.SLEEP);
        }
    }

    private void addActivityBasedOnPersonality(Activity activity, int frequency) {
        for (int i = 0; i < frequency; i++) {
            dailySchedule.add(activity);
        }
    }

    // Update
    public void updateAnimalCat(int hour) {
//        System.out.println(name + "--------DEBUG---");//debug
        System.out.println("Executing updates for " + name);
        dayUpdate(hour);
    }

    // Update performed on each day
    private void dayUpdate(int hour) {
        // Check if the cat is alive
        if (!isAlive) {
            System.out.println(name + " has died");
            System.out.println("May the force be with "+ name);
            return;
        }

        // Decrement timeToLive each hour
        timeToLive--;

        if (timeToLive <= 0){
            System.out.println(name + " has died, from an old age");
            System.out.println("May the force be with "+ name);
            isAlive = false;
            return;
        }

        if (hour == 0) {
            // Reset the cat's currentlyDoing activity + remaining frequency
            currentlyDoing = null;
            regenerateDailySchedule();

        }

        // Check if the cat is currently doing an activity
        if (currentlyDoing == null) {
            // The cat is not doing any activity, so it needs to pick a new one based on its personality
            switch (personality) {
                //? add more activites to random gen
                case FRIENDLY:
//                    currentlyDoing = getRandomActivityWithRemainingFrequency(Activity.HUNT, Activity.PLAY, Activity.CUDDLE);
                    currentlyDoing = getRandomActivityWithRemainingFrequency(Activity.SLEEP, Activity.EAT, Activity.HUNT, Activity.PLAY, Activity.CUDDLE, Activity.BITE, Activity.CLEAN_ITSELF);
                    break;
                case AGGRESSIVE:
//                    currentlyDoing = getRandomActivityWithRemainingFrequency(Activity.HUNT, Activity.PLAY, Activity.CUDDLE, Activity.BITE);
                    currentlyDoing = getRandomActivityWithRemainingFrequency(Activity.SLEEP, Activity.EAT, Activity.HUNT, Activity.PLAY, Activity.CUDDLE, Activity.BITE, Activity.CLEAN_ITSELF);
                    break;
                case LAZY:
//                    currentlyDoing = getRandomActivityWithRemainingFrequency(Activity.PLAY, Activity.CUDDLE);
                    currentlyDoing = getRandomActivityWithRemainingFrequency(Activity.SLEEP, Activity.EAT, Activity.HUNT, Activity.PLAY, Activity.CUDDLE, Activity.BITE, Activity.CLEAN_ITSELF);
                    break;
            }
            changeRoom(this);
        }

        executeActivity();
//        changeRoom(this);
    }


    // Regenerate daily schedule
    private void regenerateDailySchedule() {
        dailySchedule.clear();  // Clear existing activities

        addCleanItselfActivities();
        addEatActivities();
        addPersonalityBasedActivities();
        addSleepActivities();
        Collections.shuffle(dailySchedule);
    }

    // Execute running activity (str output)
    private void executeActivity() {
        if (currentlyDoing != null) {
//            System.out.println(name + " is " + currentlyDoing.toString().toLowerCase());

            switch (currentlyDoing) {
                case EAT:
                    System.out.println(name + " is eating.");
                    break;
                case SLEEP:
                    System.out.println(name + " is sleeping.");
                    break;
                case HUNT:
                    System.out.println(name + " is hunting.");
                    break;
                case PLAY:
                    System.out.println(name + " is playing.");
                    break;
                case CUDDLE:
                    System.out.println(name + " is cuddling.");
                    break;
                case BITE:
                    System.out.println(name + " is biting.");
                    break;
                case CLEAN_ITSELF:
                    System.out.println(name + " is cleaning itself.");
                    break;
            }
            outCurrentlyDoing = currentlyDoing;
            currentlyDoing = null;
        }
    }


    // Gets random activity + checks remaining frequency
    private Activity getRandomActivityWithRemainingFrequency(Activity... activities) {
        List<Activity> availableActivities = new ArrayList<>();

        for (Activity activity : activities) {
            int remainingFrequency = getRemainingFrequency(activity);
            if (remainingFrequency > 0) {
                availableActivities.addAll(Collections.nCopies(remainingFrequency, activity));
            }
        }

        if (availableActivities.isEmpty()) {
            // No available activities
//            System.out.println(name + "--------DEBUG---");//debug
            System.out.println(name + " has died in accident");
            return null;
        }

        Activity selectedActivity = availableActivities.get(new Random().nextInt(availableActivities.size()));
        decrementRemainingFrequency(selectedActivity);
        return selectedActivity;
    }

    // Get remaining frequency for activity
    private int getRemainingFrequency(Activity activity) {
        long remaining = dailySchedule.stream().filter(a -> a == activity).count();
        return (int) remaining;
    }

    // Remaining frequency decrementor
    private void decrementRemainingFrequency(Activity activity) {
        dailySchedule.remove(activity);
    }

    // Helpers
    public void setState(HealthState healthState) {
        this.healthState = healthState;
    }

    public boolean isHealthy() {
        return healthState.getClass() == Healthy.class;
    }

    public void nextHealthState() {
        this.healthState.changeToNextState();
    }

    public List<Activity> getDailySchedule() {
        return dailySchedule;
    }

    public Personality getPersonality() {
        return personality;
    }


    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    public String toString() {
        String aliveStatus = isAlive ? "Alive" : "Dead";
        return name + ": {Location: " + location.getName() + ", Activity: " + outCurrentlyDoing.name() + ", Status: " + aliveStatus + "}";
    }


    // Activity enum
    public enum Activity {
        EAT,
        SLEEP,
        HUNT,
        PLAY,
        CUDDLE,
        BITE,
        CLEAN_ITSELF,
    }

    private void changeRoom(AnimalCat cat){
        Random random = new Random();
        int randomNumber = random.nextInt(6);

        switch (cat.currentlyDoing){
            case EAT:
                cat.location = home.getRoom("Kitchen");
                break;
            case HUNT:
                cat.location = home.getRoom("Living room");
                cat.location = Outside.getInstance();
                break;
            case SLEEP:
            case PLAY:
            case BITE:
            case CUDDLE:
            case CLEAN_ITSELF:
                switch (randomNumber) {
                    case 0:
                        cat.location = home.getRoom("Kitchen");
                        break;
                    case 1:
                        cat.location = home.getRoom("Bedroom 2");
                        break;
                    case 2:
//                        cat.location = home.getRoom("Living room");
                        cat.location = Outside.getInstance();
                        break;
                    case 3:
                        cat.location = home.getRoom("Living Room");
                        break;
                    case 4:
                        cat.location = home.getRoom("Bedroom 1");
                        break;
                    case 5:
                        cat.location = home.getRoom("Bathroom");
                        break;
                    default:
                        cat.location = home.getRoom("Living Room");
                        break;
                }
                break;

            default:
                cat.location = Outside.getInstance();
        }
    }


    // Personality enum
    public enum Personality {
        FRIENDLY,
        AGGRESSIVE,
        LAZY
    }
}
