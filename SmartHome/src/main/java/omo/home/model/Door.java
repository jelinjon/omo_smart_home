package omo.home.model;

public class Door {
    private DoorState state = DoorState.CLOSED;

    public boolean isOpen(){
        return state.equals(DoorState.OPEN);
    }

    public void changeState(){
        if(isOpen()){
            state = DoorState.CLOSED;
        }
        else{
            state = DoorState.OPEN;
        }
    }

    private enum DoorState{
        OPEN,
        CLOSED
    }

    public String toString(){
        return "Door, state: " + state.name();
    }
}
