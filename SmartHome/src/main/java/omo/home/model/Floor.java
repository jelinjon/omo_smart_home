package omo.home.model;

import omo.home.model.rooms.Room;

import java.util.ArrayList;
import java.util.List;

public class Floor {
    private static int floorIndexCounter = 0;
    private int floorIndex;
    private List<Room>rooms;

    public void addRoom(Room room){
        this.rooms.add(room);
    }
    public List<Room> getRooms(){
        return rooms;
    }

    public Floor() {
        this.floorIndex = floorIndexCounter++;  // Assign the current value of the static counter to the floor index
        this.rooms = new ArrayList<>();
    }

    public void updateFloor(int hour){
        for(Room r : rooms){
            r.updateRoom(hour);
        }
    }

    public String toString(){
        return formatFloorNumber(floorIndex) + " floor" +
                "\n     rooms: {" + roomListFormatter() + "}";
    }

    private String roomListFormatter(){
        StringBuilder sb = new StringBuilder();
        for (Room r: rooms) {
            sb.append(r.toString());
        }
        return sb.toString();
    }
    private static String formatFloorNumber(int floorNumber) {
        if(floorNumber == 0){ return "ground";}
        if (floorNumber >= 11 && floorNumber <= 13) {
            return floorNumber + "th"; // 11th, 12th, 13th are exceptions
        }

        switch (floorNumber % 10) {
            case 1:
                return floorNumber + "st";
            case 2:
                return floorNumber + "nd";
            case 3:
                return floorNumber + "rd";
            default:
                return floorNumber + "th";
        }
    }
}
