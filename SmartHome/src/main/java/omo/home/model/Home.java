package omo.home.model;

import lombok.Data;
import omo.home.model.Animal.AnimalCat;
import omo.home.model.appliances.Appliance;
import omo.home.model.observerUtils.ClockObservable;
import omo.home.model.observerUtils.Observable;
import omo.home.model.observerUtils.Observer;
import omo.home.model.people.Person;
import omo.home.model.rooms.Outside;
import omo.home.model.rooms.Room;
import omo.home.model.sportEquipment.Bike;
import omo.home.model.sportEquipment.Ski;
import omo.home.model.sportEquipment.SportEquipment;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * represents the house
 * keeps a list of floors, people, observables, current day and time of day
 * singleton
 */
@Data
public class Home {
    private static Home instance;
    //util
    private int hourOfTheDay = 0;
    private weekDay currentDay = weekDay.MONDAY;
    private static List<Floor> floors;
    private List<Person> people;
    private List<AnimalCat> cats;
    private List<Observable> observables;
    private List<SportEquipment> sportEquipment;
    private static Logger logger = Logger.getLogger(Home.class.getName());

    private Home(){
        people = new ArrayList<>();
        cats = new ArrayList<>();
        floors = new ArrayList<>();
        observables = new ArrayList<>();
        sportEquipment = new ArrayList<>();
        logger.setLevel(Level.OFF);
    }

    public void setLoggerLevel(Level level){
        this.logger.setLevel(level);
    }

    // NESTED BUILDER CLASS
    public static class HomeBuilder {
        private Home home;

        public HomeBuilder() {
            this.home = Home.getHome();
        }

        public HomeBuilder addFloors(List<Floor> floors) {
            home.floors.addAll(floors);
            return this;
        }

        /**
         * @param value - true for logging, false for no logging
         */
        public HomeBuilder setLogging(boolean value){
            if (value) {
                home.setLoggerLevel(Level.INFO);
            }
            else{
                home.setLoggerLevel(Level.OFF);
            }
            return this;
        }

        private HomeBuilder addPeople(List<Person> people) {
            home.people.addAll(people);
            return this;
        }

        public HomeBuilder addCat(List<AnimalCat> cats) {
            home.cats.addAll(cats);
            return this;
        }

        private HomeBuilder addObservables(List<Observable> observables) {
            home.observables.addAll(observables);
            return this;
        }
        private HomeBuilder addSportEquipment(List<SportEquipment> sportEquipment) {
            home.sportEquipment.addAll(sportEquipment);
            return this;
        }

        private Home build() {
            return home;
        }
    }

    //NESTED FACTORY CLASS
    public static class HomeFactory {
        private Home home;

        /**
         *
         * @param param int 1 or 2
         * @return one of two prepared house configurations
         */
        public Home createHome(int param){
            switch (param) {
                case 1:
                    return makeConfig1();
                case 2:
                    return makeConfig2();
                default:
                    throw new IllegalArgumentException("Available parameters are 1, 2, not " + param);
            }
        }
        private Home makeConfig1(){
            home = Home.getHome();
            home.setCurrentDay(weekDay.MONDAY);

            //room init
            List<Room>rooms1 = new ArrayList<>();
            List<Observer> observers = new ArrayList<>();
            Appliance.ApplianceFactory appFact = new Appliance.ApplianceFactory();

            //kitchen
            Room kitchen = new Room.RoomBuilder()
                    .setName("Kitchen")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            Window tempWindow = new Window(kitchen);
            observers.add(tempWindow.getWindowBlinds());
            kitchen.addWindow(tempWindow);
            kitchen.addAppliance(appFact.createAppliance("stove", kitchen));
            kitchen.addAppliance(appFact.createAppliance("oven", kitchen));
            kitchen.addAppliance(appFact.createAppliance("fridge", kitchen));
            kitchen.addAppliance(appFact.createAppliance("dishwasher", kitchen));

            rooms1.add(kitchen);

            //bathroom
            Room bathroom = new Room.RoomBuilder()
                    .setName("Bathroom")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            tempWindow = new Window(bathroom);
            observers.add(tempWindow.getWindowBlinds());
            bathroom.addWindow(tempWindow);
            bathroom.addAppliance(appFact.createAppliance("washing machine", bathroom));
            bathroom.addAppliance(appFact.createAppliance("heater", bathroom));

            rooms1.add(bathroom);

            //bedroom
            Room bedroom = new Room.RoomBuilder()
                    .setName("Bedroom 1")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            tempWindow = new Window(bedroom);
            observers.add(tempWindow.getWindowBlinds());
            bedroom.addWindow(tempWindow);
            bedroom.addAppliance(appFact.createAppliance("computer", bedroom));
            bedroom.addAppliance(appFact.createAppliance("phone", bedroom));
            bedroom.addAppliance(appFact.createAppliance("heater", bedroom));
            rooms1.add(bedroom);

            //bedroom
            Room bedroom2 = new Room.RoomBuilder()
                    .setName("Bedroom 2")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            tempWindow = new Window(bedroom2);
            observers.add(tempWindow.getWindowBlinds());
            bedroom2.addWindow(tempWindow);
            bedroom2.addAppliance(appFact.createAppliance("computer", bedroom2));
            bedroom2.addAppliance(appFact.createAppliance("tv", bedroom2));
            bedroom2.addAppliance(appFact.createAppliance("heater", bedroom2));
            rooms1.add(bedroom2);

            //living room
            Room livingRoom = new Room.RoomBuilder()
                    .setName("Living room")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            tempWindow = new Window(livingRoom);
            observers.add(tempWindow.getWindowBlinds());
            livingRoom.addWindow(tempWindow);
            livingRoom.addAppliance(appFact.createAppliance("tv", livingRoom));
            livingRoom.addAppliance(appFact.createAppliance("heater", livingRoom));
            rooms1.add(livingRoom);


            //floors init
            List<Floor> floors1 = new ArrayList<>();
            while(!rooms1.isEmpty()){

                Floor tempFloor = new Floor();
                //add rooms one at a time to floors, 3 rooms per floor max
                for(int i = 0; i < 3; ++i){

                    if(rooms1.isEmpty()){
                        break;
                    }

                    tempFloor.addRoom(rooms1.get(0));
                    rooms1.remove(0);
                }
                floors1.add(tempFloor);
            }

            //people init
            List<Person>people1 = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                people1.add(new Person.PersonBuilder()
                        .addHome(home)
                        .addName("Person " + i)
                        .addOwnedBedroom(getBedrooms(floors1).get(ThreadLocalRandom.current().nextInt(0, getBedrooms(floors1).size())))
                        .addWakeUpHour(ThreadLocalRandom.current().nextInt(5, 8))
                        .addGoToSleepHour(ThreadLocalRandom.current().nextInt(22, 24))
                        .build());
            }

            //sport equipment init
            List<SportEquipment>equipmentList = new ArrayList<>();
            equipmentList.add(new Bike(Outside.getInstance()));
            equipmentList.add(new Ski(Outside.getInstance()));

            // 3 Cat initialization Version07 can die
            List<AnimalCat> cats1 = new ArrayList<>();
            String[] names = {"Garfield", "Rouge", "Thomas Becket"};
            String[] personalities = {"FRIENDLY", "LAZY", "AGGRESSIVE"};

            for (int i = 0; i < 3; i++) {
                String current_personality = personalities[i];
                cats1.add(new AnimalCat.AnimalCatBuilder()
                        .addName(names[i])
                        .addHome(home)
                        .addPersonality(AnimalCat.Personality.valueOf(current_personality))
                        .setTimeToLive()
                        .build());
            }

            //observables init
            List<Observable>obs1 = new ArrayList<>();
            ClockObservable cl = new ClockObservable();

            for (Observer o : observers) {
                cl.addObserver(o);
            }
            obs1.add(cl);

            home = new HomeBuilder()
                    .addFloors(floors1)
                    .addPeople(people1)
                    .addCat(cats1)
                    .addObservables(obs1)
                    .addSportEquipment(equipmentList)
                    .setLogging(false)
                    .build();

            return home;
        }

        private Home makeConfig2(){
            home = Home.getHome();
            home.setCurrentDay(weekDay.MONDAY);

            //room init
            List<Room>rooms1 = new ArrayList<>();
            List<Observer> observers = new ArrayList<>();
            Appliance.ApplianceFactory appFact = new Appliance.ApplianceFactory();

            //kitchen
            Room kitchen = new Room.RoomBuilder()
                    .setName("Kitchen")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            Window tempWindow = new Window(kitchen);
            observers.add(tempWindow.getWindowBlinds());
            kitchen.addWindow(tempWindow);
            kitchen.addAppliance(appFact.createAppliance("stove", kitchen));
            kitchen.addAppliance(appFact.createAppliance("oven", kitchen));
            kitchen.addAppliance(appFact.createAppliance("fridge", kitchen));
            kitchen.addAppliance(appFact.createAppliance("dishwasher", kitchen));

            rooms1.add(kitchen);

            //bathroom
            Room bathroom = new Room.RoomBuilder()
                    .setName("Bathroom")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            tempWindow = new Window(bathroom);
            observers.add(tempWindow.getWindowBlinds());
            bathroom.addWindow(tempWindow);
            bathroom.addAppliance(appFact.createAppliance("washing machine", bathroom));
            bathroom.addAppliance(appFact.createAppliance("heater", bathroom));

            rooms1.add(bathroom);

            //bedroom
            Room bedroom = new Room.RoomBuilder()
                    .setName("Bedroom 1")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            tempWindow = new Window(bedroom);
            observers.add(tempWindow.getWindowBlinds());
            bedroom.addWindow(tempWindow);
            bedroom.addAppliance(appFact.createAppliance("computer", bedroom));
            bedroom.addAppliance(appFact.createAppliance("phone", bedroom));
            bedroom.addAppliance(appFact.createAppliance("heater", bedroom));
            rooms1.add(bedroom);

            //bedroom 2
            Room bedroom2 = new Room.RoomBuilder()
                    .setName("Bedroom 2")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            tempWindow = new Window(bedroom2);
            observers.add(tempWindow.getWindowBlinds());
            bedroom2.addWindow(tempWindow);
            bedroom2.addAppliance(appFact.createAppliance("computer", bedroom2));
            bedroom2.addAppliance(appFact.createAppliance("tv", bedroom2));
            bedroom2.addAppliance(appFact.createAppliance("heater", bedroom2));
            rooms1.add(bedroom2);

            //bathroom 2
            Room bathroom2 = new Room.RoomBuilder()
                    .setName("Bathroom")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            tempWindow = new Window(bathroom2);
            observers.add(tempWindow.getWindowBlinds());
            bathroom2.addWindow(tempWindow);
            bathroom2.addAppliance(appFact.createAppliance("washing machine", bathroom2));
            bathroom2.addAppliance(appFact.createAppliance("heater", bathroom2));
            rooms1.add(bathroom2);

            //bedroom 3
            Room bedroom3 = new Room.RoomBuilder()
                    .setName("Bedroom 1")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            //2 windows
            tempWindow = new Window(bedroom3);
            observers.add(tempWindow.getWindowBlinds());
            bedroom3.addWindow(tempWindow);

            tempWindow = new Window(bedroom3);
            observers.add(tempWindow.getWindowBlinds());
            bedroom3.addWindow(tempWindow);
            bedroom3.addAppliance(appFact.createAppliance("computer", bedroom3));
            bedroom3.addAppliance(appFact.createAppliance("phone", bedroom3));
            bedroom3.addAppliance(appFact.createAppliance("heater", bedroom3));
            rooms1.add(bedroom3);

            //living room
            Room livingRoom = new Room.RoomBuilder()
                    .setName("Living room")
                    .addDoor(new Door())
//                    .addFurniture()
                    .addLight(new Lights())
                    .build();

            tempWindow = new Window(livingRoom);
            observers.add(tempWindow.getWindowBlinds());
            livingRoom.addWindow(tempWindow);
            livingRoom.addAppliance(appFact.createAppliance("tv", livingRoom));
            livingRoom.addAppliance(appFact.createAppliance("heater", livingRoom));
            rooms1.add(livingRoom);


            //floors init
            List<Floor> floors1 = new ArrayList<>();
            while(!rooms1.isEmpty()){

                Floor tempFloor = new Floor();
                //add rooms one at a time to floors, 2 rooms per floor max
                for(int i = 0; i < 2; ++i){

                    if(rooms1.isEmpty()){
                        break;
                    }

                    tempFloor.addRoom(rooms1.get(0));
                    rooms1.remove(0);
                }
                floors1.add(tempFloor);
            }

            //people init
            List<Person>people1 = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                people1.add(new Person.PersonBuilder()
                        .addHome(home)
                        .addName("Person " + i)
                        .addOwnedBedroom(getBedrooms(floors1).get(ThreadLocalRandom.current().nextInt(0, getBedrooms(floors1).size())))
                        .addWakeUpHour(ThreadLocalRandom.current().nextInt(5, 8))
                        .addGoToSleepHour(ThreadLocalRandom.current().nextInt(22, 24))
                        .build());
            }

            //sport equipment init
            List<SportEquipment>equipmentList = new ArrayList<>();
            equipmentList.add(new Bike(Outside.getInstance()));
            equipmentList.add(new Bike(Outside.getInstance()));
            equipmentList.add(new Bike(Outside.getInstance()));
            equipmentList.add(new Ski(Outside.getInstance()));
            equipmentList.add(new Ski(Outside.getInstance()));
            equipmentList.add(new Ski(Outside.getInstance()));
            equipmentList.add(new Ski(Outside.getInstance()));

            // 3 Cat initialization Version07 can die
            List<AnimalCat> cats1 = new ArrayList<>();
            String[] names = {"Luna", "Milo", "Oliver", "Leo"};
            List<String> personalities = List.of(new String[]{"FRIENDLY", "LAZY", "AGGRESSIVE"});

            for (int i = 0; i < 3; i++) {
                String current_personality = personalities.get(new Random().nextInt(personalities.size()));
                cats1.add(new AnimalCat.AnimalCatBuilder()
                        .addName(names[i])
                        .addHome(home)
                        .addPersonality(AnimalCat.Personality.valueOf(current_personality))
                        .setTimeToLive()
                        .build());
            }

            //observables init
            List<Observable>obs1 = new ArrayList<>();
            ClockObservable cl = new ClockObservable();

            for (Observer o : observers) {
                cl.addObserver(o);
            }
            obs1.add(cl);

            home = new HomeBuilder()
                    .addFloors(floors1)
                    .addPeople(people1)
                    .addCat(cats1)
                    .addObservables(obs1)
                    .addSportEquipment(equipmentList)
                    .setLogging(false)
                    .build();

            return home;
        }

    }

    /**
     *
     * @param name desired equipment Class name as String
     * @return first equipment not used that fits requirements
     */
    public SportEquipment getSportEquipment(String name){
        for (SportEquipment sp : sportEquipment){
            if(sp.getClass().getSimpleName().equals(name)){
                if(!sp.getBeingUsed()){
                    return sp;
                }
            }
        }
        return null;
    }

    /**
     *
     * @param floors takes Floors as param
     * @return a list of all rooms on Floors provided
     */
    public static List<Room> getRooms(List<Floor> floors){
        List<Room> res = new ArrayList<>();
        for (Floor f:floors) {
            res.addAll(f.getRooms());
        }
        return res;
    }

    /**
     *
     * @param floors takes list of Floors as param
     * @return all bedrooms found in the Floors provided
     */
    private static List<Room> getBedrooms(List<Floor> floors){
        return getRooms(floors).stream().filter(room -> room.getName().startsWith("Bedroom")).collect(Collectors.toList());
    }

    /**
     *
     * @param roomType the desired room type as string name of Class
     * @return the first room that fits requirements
     */
    public Room getRoom(String roomType){
        for (Room r:getRooms(floors)) {
            if(Objects.equals(r.getName(), roomType)){
                return r;
            }
        }
        return Outside.getInstance();
    }


    //singleton - returns instance
    public static Home getHome(){
        if(instance == null){
            instance = new Home();
        }
        return instance;
    }

    public boolean isWeekend(){
        return currentDay == weekDay.SATURDAY || currentDay == weekDay.SUNDAY;
    }

    //CENTRAL UPDATE METHOD
    public void updateState(){
        System.out.println("#-------------------------------------------------------#");
        System.out.println("Executing update, current hour is: " +
                LocalTime.of(hourOfTheDay, 0)
                        .format(DateTimeFormatter.ofPattern("HH:mm a", Locale.ENGLISH)) + ", day: " + currentDay);
        System.out.println("#-------------------------------------------------------#");

        //Person updates
        for(Person p : people){
            p.updatePerson(hourOfTheDay);
        }
        // Cat updates
        for(AnimalCat cat : cats){
            cat.updateAnimalCat(hourOfTheDay);
        }
        //floor updates
        for(Floor f : floors){
            f.updateFloor(hourOfTheDay);
        }
        //observables updates
        for(Observable o : observables){
            o.update(hourOfTheDay);
        }

        for(SportEquipment se : sportEquipment){
            se.updateSportEqipment(hourOfTheDay);
        }

        hourOfTheDay++;
        if(hourOfTheDay > 23){
            hourOfTheDay = 0;
            currentDay = currentDay.nextDay();
        }

        //debug log print of floor and room disposition plus people location and current activity
        StringBuilder sb = new StringBuilder();
        sb.append("\nhome contains: \n");
        for (Floor f : floors) {
            sb.append(f.toString());
            sb.append("\n");
        }
        for (Person p : people) {
            sb.append(p.toString());
            sb.append("\n");
        }
        for(SportEquipment se : sportEquipment){
            sb.append(se.toString());
            sb.append("\n");
        }
        logger.info(sb.toString());
    }


    //WEEK DAY ENUM WITH TRANSITION METHODS
    public enum weekDay {
        SUNDAY {
            @Override
            public weekDay nextDay() {
                return MONDAY;
            }
        },
        MONDAY {
            @Override
            public weekDay nextDay() {
                return TUESDAY;
            }
        },
        TUESDAY {
            @Override
            public weekDay nextDay() {
                return WEDNESDAY;
            }
        },
        WEDNESDAY {
            @Override
            public weekDay nextDay() {
                return THURSDAY;
            }
        },
        THURSDAY {
            @Override
            public weekDay nextDay() {
                return FRIDAY;
            }
        },
        FRIDAY {
            @Override
            public weekDay nextDay() {
                return SATURDAY;
            }
        },
        SATURDAY {
            @Override
            public weekDay nextDay() {
                return SUNDAY;
            }
        };

        /**
         * abstract method for day transitions
         */
        public abstract weekDay nextDay();
    }
}

