package omo.home.model;

import omo.home.model.rooms.Room;

public class Lights {
    private LightsState state = LightsState.OFF;

    private enum LightsState{
        ON,
        OFF;
    }

    public String toString(){
        return "Lights, state: " + state.name();
    }
}
