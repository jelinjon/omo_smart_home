package omo.home.model;

import omo.home.model.lighting.WindowBlinds;
import omo.home.model.rooms.Room;

public class Window {
    private static int windowIndexCounter = 0;
    private int windowIndex;
    private WindowBlinds windowBlinds;
    private Room room;

    public Window(Room room){
        this.windowIndex = windowIndexCounter++;  // Assign the current value of the static counter to the window index

        this.room = room;
        this.windowBlinds = new WindowBlinds(this);
    }
    public WindowBlinds getWindowBlinds(){
        return windowBlinds;
    }

    public int getWindowIndex() {
        return windowIndex;
    }

    public String toString(){
        return "window " + windowIndex + ": {" + windowBlinds.toString() + "}";
    }

}
