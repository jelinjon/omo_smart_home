package omo.home.model.appliances;

import omo.home.model.rooms.Room;

public abstract class Appliance {
    private Room room;

    //in watts per hour
    protected int powerConsumptionActive;
    protected int powerConsumptionInactive;
    protected int powerConsumptionIdle;

    protected State state;

    protected enum State{
        ACTIVE,
        IDLE,
        INACTIVE
    }

    public Appliance(Room room){
        this.room = room;
        this.state = State.INACTIVE;
    }

    public void updateAppliance(int hour){
//        System.out.println(this.getClass().getSimpleName() + " in room " + room.getName());
        System.out.println("Appliance: {class: " + this.getClass().getSimpleName()
                + ", room: " + room.getName()
                + ", state: " + state.name()
                + ", consuption: " + getConsumption() + "}");
    }

    public float getConsumption(){
        switch (state){
            case ACTIVE:
                return powerConsumptionActive;
            case IDLE:
                return powerConsumptionIdle;
            default:
                return powerConsumptionInactive;
        }
    }

    public void switchON(){
        state = State.ACTIVE;
    }

    public void switchOFF(){
        state = State.INACTIVE;
    }

    public static class ApplianceFactory {
        /**
         * use a parameter type to define desired apppliance, defined params are
         * <pre>
         * - computer
         * - dishwasher
         * - fridge
         * - heater
         * - oven
         * - phone
         * - stove
         * - tv
         * - washing machine
         * </pre>
         * @param type type of appliance
         * @param room location of the appliance
         * @return
         */
        public Appliance createAppliance(String type, Room room) {
            switch (type.toLowerCase()) {
                case "computer":
                    return new Computer(room);
                case "dishwasher":
                    return new Dishwasher(room);
                case "fridge":
                    return new Fridge(room);
                case "heater":
                    return new Heater(room);
                case "oven":
                    return new Oven(room);
                case "phone":
                    return new Phone(room);
                case "stove":
                    return new Stove(room);
                case "tv":
                    return new TV(room);
                case "washing machine":
                    return new WashingMachine(room);
                default:
                    throw new IllegalArgumentException("Unsupported appliance type: " + type);
            }
        }
    }

    // Abstract method for performing appliance-specific action
    public abstract void performAction();
}
