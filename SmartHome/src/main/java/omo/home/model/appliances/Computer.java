package omo.home.model.appliances;

import omo.home.model.rooms.Room;

public class Computer extends Appliance{
    public Computer(Room room) {
        super(room);
        this.powerConsumptionActive = 350;
        this.powerConsumptionIdle = 75;
        this.powerConsumptionInactive = 3;
    }

    @Override
    public void performAction() {
        System.out.println("Using the computer.");
    }

    @Override
    public void updateAppliance(int hour) {
        if(hour == 7){
            this.state = State.IDLE;
        }
        if(hour == 8){
            this.state = State.ACTIVE;
        }
        if(hour == 11){
            this.state = State.INACTIVE;
        }
        super.updateAppliance(hour);
    }
}
