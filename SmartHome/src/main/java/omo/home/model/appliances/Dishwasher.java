package omo.home.model.appliances;

import omo.home.model.rooms.Room;

import java.util.Random;

public class Dishwasher extends Appliance {
    public Dishwasher(Room room) {
        super(room);
        this.powerConsumptionActive = 1600;
        this.powerConsumptionIdle = 20;
        this.powerConsumptionInactive = 0;
    }

    @Override
    public void performAction() {
        System.out.println("Washing dishes in the dishwasher.");
    }

    @Override
    public void updateAppliance(int hour) {
        if(hour == 20){
            this.state = State.ACTIVE;
        }
        else if((new Random().nextDouble() * 100.0 < 30.0) && hour == 21){
            this.state = State.ACTIVE;
        }
        else{
            this.state = State.INACTIVE;
        }
        super.updateAppliance(hour);
    }
}
