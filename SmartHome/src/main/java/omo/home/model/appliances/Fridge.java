package omo.home.model.appliances;

import omo.home.model.rooms.Room;

public class Fridge extends Appliance{
    public Fridge(Room room) {
        super(room);
        this.powerConsumptionActive = 400;
        this.powerConsumptionIdle = 75;
        this.powerConsumptionInactive = 0;
        this.state = State.ACTIVE;
    }

    @Override
    public void performAction() {
        System.out.println("Storing food in the fridge.");
    }

    //no update, stays active always
}
