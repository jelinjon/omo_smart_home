package omo.home.model.appliances;

import omo.home.model.rooms.Room;

public class Heater extends Appliance{
    public Heater(Room room) {
        super(room);
        this.powerConsumptionActive = 3000;
        this.powerConsumptionIdle = 3;
        this.powerConsumptionInactive = 0;
    }

    @Override
    public void performAction() {
        System.out.println("Heating up.");
    }


    @Override
    public void updateAppliance(int hour) {
        if((hour >= 6 && hour < 9) || (hour >= 16 && hour < 22)){
            this.state = State.ACTIVE;
        }
        else{
            this.state = State.INACTIVE;
        }
        super.updateAppliance(hour);
    }
}
