package omo.home.model.appliances;

import omo.home.model.rooms.Room;

public class Oven extends Appliance {
    public Oven(Room room) {
        super(room);
        this.powerConsumptionActive = 3500;
        this.powerConsumptionIdle = 5;
        this.powerConsumptionInactive = 0;
    }

    @Override
    public void performAction() {
        System.out.println("Baking a dish in the oven.");
    }

    @Override
    public void updateAppliance(int hour) {
        if(hour >= 17 && hour <= 18){
            this.state = State.ACTIVE;
        }
        else{
            this.state = State.INACTIVE;
        }
        super.updateAppliance(hour);
    }
}
