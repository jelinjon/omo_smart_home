package omo.home.model.appliances;

import omo.home.model.rooms.Room;

public class Phone extends Appliance{
    public Phone(Room room) {
        super(room);
        this.powerConsumptionActive = 12;
        this.powerConsumptionIdle = 3;
        this.powerConsumptionInactive = 1;
    }

    @Override
    public void performAction() {
        System.out.println("Using the phone.");
    }

    @Override
    public void updateAppliance(int hour) {
        if(hour >= 22){
            this.state = State.ACTIVE;
        }
        else{
            this.state = State.INACTIVE;
        }
        super.updateAppliance(hour);
    }
}
