package omo.home.model.appliances;

import omo.home.model.rooms.Room;

import java.util.Random;

public class Stove extends Appliance{
    public Stove(Room room) {
        super(room);
        this.powerConsumptionActive = 2000;
        this.powerConsumptionIdle = 30;
        this.powerConsumptionInactive = 0;
    }

    @Override
    public void performAction() {
        System.out.println("Turning on the stove for cooking.");
    }

    @Override
    public void updateAppliance(int hour) {
        if((new Random().nextDouble() * 100.0 < 30.0) && hour == 7){
            this.state = State.ACTIVE;
        }
        else if(hour == 17){
            this.state = State.ACTIVE;
        }
        else{
            this.state = State.INACTIVE;
        }
        super.updateAppliance(hour);
    }
}
