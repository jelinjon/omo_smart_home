package omo.home.model.appliances;

import omo.home.model.rooms.Room;

import java.util.Random;

public class TV extends Appliance{
    public TV(Room room) {
        super(room);
        this.powerConsumptionActive = 250;
        this.powerConsumptionIdle = 5;
        this.powerConsumptionInactive = 1;
    }

    @Override
    public void performAction() {
        System.out.println("Watching the tv.");
    }

    @Override
    public void updateAppliance(int hour) {
        if((new Random().nextDouble() * 100.0 < 30.0) && hour == 8){
            this.state = State.ACTIVE;
        }
        else if(hour >= 18 && hour < 23){
            this.state = State.ACTIVE;
        }
        else{
            this.state = State.INACTIVE;
        }
        super.updateAppliance(hour);
    }
}
