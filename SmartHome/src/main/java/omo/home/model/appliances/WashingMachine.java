package omo.home.model.appliances;

import omo.home.model.rooms.Room;

public class WashingMachine extends Appliance {
    public WashingMachine(Room room) {
        super(room);
        this.powerConsumptionActive = 1250;
        this.powerConsumptionIdle = 15;
        this.powerConsumptionInactive = 0;
    }


    @Override
    public void performAction() {
        System.out.println("Washing clothes in the washing machine.");
    }

    @Override
    public void updateAppliance(int hour) {
        if(hour == 15){
            this.state = State.ACTIVE;
        }
        else{
            this.state = State.INACTIVE;
        }
        super.updateAppliance(hour);
    }
}
