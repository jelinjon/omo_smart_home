package omo.home.model.lighting;

import omo.home.model.Window;
import omo.home.model.observerUtils.ClockObservable;
import omo.home.model.observerUtils.Observable;
import omo.home.model.observerUtils.Observer;

import java.security.PublicKey;


public class WindowBlinds implements Observer {
    private BlindsState state = BlindsState.CLOSED;
    private final Window window;

    public WindowBlinds(Window window){
        this.window = window;
    }

    @Override
    public void update(Observable observable) {
        if (observable instanceof ClockObservable) {
            ClockObservable clock = (ClockObservable) observable;
            int currentHour = clock.getCurrentHour();

            // Adjust blinds based on the hour of the day
            adjustBlindsBasedOnHour(currentHour);
        }
    }

    private void adjustBlindsBasedOnHour(int currentHour) {
        if (currentHour >= 6 && currentHour < 11) {
            System.out.println("Blinds at window " + window.getWindowIndex() + " Opening"); // It's daytime, open blinds
            state = BlindsState.OPEN;
        }
        else if (currentHour >= 11 && currentHour < 15){
            System.out.println("Blinds at window " + window.getWindowIndex() + " Drawing"); // Its midday, draw the blinds
            state = BlindsState.DRAWN;
        }
        else {
            System.out.println("Blinds at window" + window.getWindowIndex() + " Closing"); // It's nighttime, close blinds
            state = BlindsState.CLOSED;
        }
    }

    /**
     * describes the state of the blinds
     * DRAWN means drawn up? in case it gets too windy or smth
     */
    private enum BlindsState{
        CLOSED,
        OPEN,
        DRAWN;
    }

    public String toString(){
        return "Window Blinds, state: " + state.name();
    }
}
