package omo.home.model.observerUtils;

import omo.home.model.observerUtils.Observable;

// Observable class representing a light sensor
public class ClockObservable extends Observable {
    private int currentHour;

    public int getCurrentHour() {
        return currentHour;
    }

    @Override
    public void update(int hour) {
        // Update logic based on the change in hour
        currentHour = hour;
        System.out.println("Clock is updating, current hour is: " + currentHour);
        notifyObservers();
    }
}
