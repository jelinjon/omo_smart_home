package omo.home.model.observerUtils;

public interface Observer {
    void update(Observable observable);
}
