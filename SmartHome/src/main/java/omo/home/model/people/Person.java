package omo.home.model.people;

import omo.home.model.Home;
import omo.home.model.people.state.HealthContext;
import omo.home.model.people.state.Healthy;
import omo.home.model.people.state.HealthState;
import omo.home.model.people.strategy.DayStrategy;
import omo.home.model.people.strategy.WeekStrategy;
import omo.home.model.people.strategy.WeekendStrategy;
import omo.home.model.rooms.Outside;
import omo.home.model.rooms.Room;
import omo.home.model.sportEquipment.SportEquipment;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Person implements HealthContext {
    //attributes
    private Room location;
    private Room ownedBedroom;
    private boolean isAsleep = true;
    private int wakeUpTime;
    private int goToSleepTime;
    private String name = "unnamed person";
    private Activity currentlyDoing = Activity.NOTHING;
    private int hoursTillNextActivity = 0;
    private List<Activity> dailySchedule;
    private HealthState healthState = new Healthy(this);
    private DayStrategy strategy = new WeekStrategy(this);

    private static Home home;
    private SportEquipment usedEquipment;

    //constructor
    public Person(){

    }

    //builder for Person class
    private Person(PersonBuilder builder) {
        this.name = builder.name;
        this.location = builder.location;
        this.ownedBedroom = builder.location;
        home = builder.home;
        this.goToSleepTime = builder.goToSleepTime;
        this.wakeUpTime = builder.wakeUpTime;
    }
    public static class PersonBuilder {
        private Room location;
        private String name;
        private Home home;
        private int goToSleepTime;
        private int wakeUpTime;

        public PersonBuilder addName(String name) {
            this.name = name;
            return this;
        }

        public PersonBuilder addOwnedBedroom(Room room){
            this.location = room;
            return this;
        }

        public PersonBuilder addHome(Home home){
            this.home = home;
            return this;
        }

        public PersonBuilder addWakeUpHour(int hour) {
            this.wakeUpTime = hour;
            return this;
        }

        public PersonBuilder addGoToSleepHour(int hour) {
            this.goToSleepTime = hour;
            return this;
        }

        public Person build() {
            return new Person(this);
        }
    }

    //methods

    public void setState(HealthState healthState) {
        this.healthState = healthState;
    }

    public boolean isHealthy() {
        return healthState.getClass() == Healthy.class;
    }
    public void nextHealthState() { this.healthState.changeToNextState();}
    public static Home getHome(){
        return home;
    }
    public String getName() {return name;
    }

    public void updatePerson(int hour){

        System.out.println("Executing updates for " + name);

        //swap week and weekend strategies if necessary
        if(home.isWeekend() && strategy instanceof WeekStrategy){
            strategy = new WeekendStrategy(this);
        }
        if(!home.isWeekend() && strategy instanceof WeekendStrategy){
            strategy = new WeekStrategy(this);
        }

        //generate new schedule for the following day at midnight, according to current strategy
        if(hour == 0){
            this.dailySchedule = strategy.planTheDay(24- (24 - goToSleepTime + wakeUpTime));
        }

        //swap activities, when previous is finished
        if (hoursTillNextActivity == 0 && !isAsleep) {
            if((currentlyDoing.equals(Activity.BIKE) || currentlyDoing.equals(Activity.SKI)) && usedEquipment != null){
                usedEquipment.setUser();
            }

            if (!dailySchedule.isEmpty()) {

                this.currentlyDoing = dailySchedule.get(0);
                currentlyDoing.changeRoom(this);
                this.hoursTillNextActivity = currentlyDoing.getDuration();
                dailySchedule.remove(0);

                if(currentlyDoing.equals(Activity.BIKE)){
                    //find if used, then start using
                    SportEquipment temp = home.getSportEquipment("Bike");

                    if (temp != null) {
                        if(!temp.getBeingUsed()){
                            temp.setUser(this);
                            usedEquipment = temp;
                        }
                    }
                }
                if(currentlyDoing.equals(Activity.SKI)){
                    //find if used, then start using
                    SportEquipment temp = home.getSportEquipment("Ski");

                    if (temp != null) {
                        if(!temp.getBeingUsed()){
                            temp.setUser(this);
                            usedEquipment = temp;
                        }
                    }
                }

                System.out.println("    " + name + " is currently doing: " + this.currentlyDoing);
            }
            else{
                System.out.println("    daily activities done");
            }
        }
        if(!isAsleep && hoursTillNextActivity != 0){
            hoursTillNextActivity--;
        }

        dayUpdate(hour);
    }

    private void dayUpdate(int hour){
        //possible health state change at midnight
        if (hour == 0) {

            if(isHealthy()){
                if(new Random().nextDouble() * 100.0 < 14.0){
                    nextHealthState();
                    System.out.println("    " + name + " has gotten sick");
                }
            }
            else{
                if(new Random().nextDouble() * 100.0 < 50.0){
                    nextHealthState();
                    System.out.println("    " + name + " has become healthy again");
                }
            }
        }

        //wake up logic
        if(isAsleep){
            //variate for weekend and week
            if(hour == wakeUpTime){
                isAsleep = false;
                System.out.println("    " + name + " has woken up");
            }
        }
        if(!isAsleep){
            //variate for weekend and week
            if(hour == goToSleepTime){
                isAsleep = true;
                location = ownedBedroom;
                currentlyDoing = Activity.NOTHING;
                System.out.println("    " + name + " has gone to sleep");
            }
        }
    }

    public String toString(){
        return name + ": {Location: " + location.getName() + ", Activity: " + currentlyDoing.name() + "}";
    }

    public enum Activity {
        //TWO SPECIAL CASES
        WORK(8),
        EAT(1),

        EXERCISE(1),
        READ(1),
        WATCH_TV(2),
        PLAY_VIDEO_GAMES(2),
        SOCIALIZE(3),
        COOK(1),
        CLEAN(1),
        SHOP(2),
        BIKE(2),
        SKI(6),
        GARDENING(2),
        NAP(1),
        REST(3),
        NOTHING(1),
        SHOWER(1),
        VISIT_GP(3);

        private final int duration;
        private void changeRoom(Person p){
            switch (p.currentlyDoing){
                case EAT:
                case COOK:
                    p.location = home.getRoom("Kitchen");
                    break;
                case NAP:
                case EXERCISE:
                case PLAY_VIDEO_GAMES:
                    p.location = p.ownedBedroom;
                    break;
                case READ:
                case REST:
                case NOTHING:
                case WATCH_TV:
                    p.location = home.getRoom("Living room");
                    break;
                case CLEAN:
                case SHOWER:
                    p.location = home.getRoom("Bathroom");
                    break;
                default:
                    p.location = Outside.getInstance();
            }
        }
        Activity(int duration){
            this.duration = duration;
        }
        public int getDuration() {
            return duration;
        }
        public static List<Activity> getWeekdayActivities() {
            return Arrays.asList(Activity.EXERCISE, Activity.READ, Activity.WATCH_TV,
                    Activity.PLAY_VIDEO_GAMES, Activity.SOCIALIZE, Activity.COOK, Activity.SHOWER);
        }

        public static List<Activity> getSickActivities() {
            return Arrays.asList(Activity.NAP, Activity.REST, Activity.VISIT_GP, Activity.SHOWER, Activity.SHOWER);
        }

        public static List<Activity> getWeekendActivities() {
            return Arrays.asList(Activity.EXERCISE, Activity.READ, Activity.WATCH_TV,
                    Activity.PLAY_VIDEO_GAMES, Activity.SOCIALIZE, Activity.COOK,
                    Activity.SKI, Activity.BIKE, Activity.GARDENING, Activity.SHOP, Activity.CLEAN, Activity.NAP);
        }
    }
}
