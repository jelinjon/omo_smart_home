package omo.home.model.people.state;

public interface HealthContext {
    void setState(HealthState healthState);
}
