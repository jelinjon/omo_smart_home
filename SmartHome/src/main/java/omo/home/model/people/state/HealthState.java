package omo.home.model.people.state;

public abstract class HealthState {
    public HealthState(HealthContext context) {
        this.context = context;
    }
    protected int hoursSpentAtHome;

    public int getHoursSpentAtHome() {
        return hoursSpentAtHome;
    }

    public void performDailyActivities() {}

    /**
     * HealthContext interface that allows change state of context from another state
     */
    protected HealthContext context;

    /**
     * Set the next state to the context
     */
    public abstract void changeToNextState();

    /**
     * Returns the context object
     */
    public HealthContext getContext() {
        return context;
    }

    /**
     * Set the context object
     */
    public void setContext(HealthContext context) {
        this.context = context;
    }
}
