package omo.home.model.people.state;

public class Healthy extends HealthState {
    public Healthy(HealthContext context){
        super(context);
        hoursSpentAtHome = 14;
    }

    @Override
    public void changeToNextState() {
        context.setState(new Sick(context));
    }

    @Override
    public void performDailyActivities() {
        System.out.println("Person is healthy. Performing regular daily activities.");
        // Additional activities for a healthy person
    }
}
