package omo.home.model.people.state;

public class Sick extends HealthState {
    public Sick(HealthContext context){
        super(context);
        hoursSpentAtHome = 24;
    }

    @Override
    public void changeToNextState() {
        context.setState(new Healthy(context));
    }

    @Override
    public void performDailyActivities() {
        System.out.println("Person is sick. Taking medication and resting.");
        // Additional activities for a sick person
    }
}
