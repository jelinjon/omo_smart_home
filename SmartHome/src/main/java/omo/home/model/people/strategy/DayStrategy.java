package omo.home.model.people.strategy;

import omo.home.model.people.Person;

import java.util.List;

public interface DayStrategy {

    public List<Person.Activity> planTheDay(int totalAwakeHours);

}
