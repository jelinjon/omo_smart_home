package omo.home.model.people.strategy;

import omo.home.model.people.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WeekStrategy implements DayStrategy{
    Person person;

    public WeekStrategy(Person person){
        this.person = person;
    }


    @Override
    public List<Person.Activity> planTheDay(int totalAwakeHours) {
        List<Person.Activity> generatedSchedule = new ArrayList<>();
        int remainingHours = totalAwakeHours;

        //change activity range depending on weekend/weekday
        List<Person.Activity> chooseFrom;
        if(!person.isHealthy()){
            chooseFrom = Person.Activity.getSickActivities();
        }
        else{
            chooseFrom = Person.Activity.getWeekdayActivities();
        }

        //eat breakfast, work, eat lunch, fill with other things, eat dinner
        generatedSchedule.add(Person.Activity.EAT);
        remainingHours -= Person.Activity.EAT.getDuration();

        generatedSchedule.add(Person.Activity.WORK);
        remainingHours -= Person.Activity.WORK.getDuration();

        generatedSchedule.add(Person.Activity.EAT);
        remainingHours -= Person.Activity.EAT.getDuration();

        while(remainingHours > 1){

//            int randIndex = (int)(System.currentTimeMillis() % chooseFrom.size());
            int randIndex = new Random().nextInt(chooseFrom.size());

            //make time to eat dinner
            if((remainingHours - chooseFrom.get(randIndex).getDuration()) < 1){
                continue;
            }

            if(chooseFrom.get(randIndex).getDuration() < remainingHours){

                generatedSchedule.add(chooseFrom.get(randIndex));
                remainingHours -= chooseFrom.get(randIndex).getDuration();
//                chooseFrom.remove(randIndex);
            }
        }

        generatedSchedule.add(Person.Activity.EAT);
        remainingHours -= Person.Activity.EAT.getDuration();

        return generatedSchedule;
    }
}
