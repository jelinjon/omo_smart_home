package omo.home.model.rooms;

public class Outside extends Room{
    private Outside(){
        this.name = "Outside";
    }
    private static Outside single_instance = null;
    public static synchronized Outside getInstance(){
        if(single_instance == null){
            single_instance = new Outside();
        }
        return single_instance;
    }
}
