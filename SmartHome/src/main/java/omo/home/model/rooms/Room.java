package omo.home.model.rooms;

import omo.home.model.Door;
import omo.home.model.Furniture;
import omo.home.model.Lights;
import omo.home.model.Window;
import omo.home.model.appliances.Appliance;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Room {
    protected String name = "unspecified room";
    private List<Appliance> appliances;
    private List<Lights> lights;
    private List<Window> windows;
    private List<Furniture> furniture;
    private List<Door> doors;

    // Nested builder class
    public static class RoomBuilder {
        private Room room;

        public RoomBuilder() {
            this.room = new Room();
        }

        public RoomBuilder addAppliance(Appliance appliance) {
            room.addAppliance(appliance);
            return this;
        }
        public RoomBuilder addWindow(Window window) {
            room.addWindow(window);
            return this;
        }

        public RoomBuilder setName(String name){
            room.setName(name);
            return this;
        }

        public RoomBuilder addLight(Lights type) {
            room.addLight(type);
            return this;
        }

        public RoomBuilder addFurniture(Furniture name) {
            room.addFurniture(name);
            return this;
        }

        public RoomBuilder addDoor(Door type) {
            room.addDoor(type);
            return this;
        }

        public Room build() {
            return room;
        }
    }

    public Room() {
        appliances = new ArrayList<>();
        lights = new ArrayList<>();
        windows = new ArrayList<>();
        furniture = new ArrayList<>();
        doors = new ArrayList<>();
    }

    public void updateRoom(int hour){
        updateAppliances(hour);
    }

    private void updateAppliances(int hour){
        for (Appliance a:appliances) {
            a.updateAppliance(hour);
        }
    }

    private void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }

    public void addAppliance(Appliance appliance) {
        appliances.add(appliance);
    }

    public void addWindow(Window window) {
        windows.add(window);
    }

    private void addLight(Lights light) {
        lights.add(light);
    }

    private void addFurniture(Furniture piece) {
        furniture.add(piece);
    }

    private void addDoor(Door door) {
        doors.add(door);
    }

    public String toString() {
        return name + ": {" +
                "Appliances: " + appliances.stream().map(a -> a.getClass().getSimpleName()).collect(Collectors.joining(", ", "{", "}")) +
                ", Lights: " + lights.stream().map(l -> l.toString()).collect(Collectors.joining(", ", "{", "}")) +
                ", Windows: " + windows.stream().map(l -> l.toString()).collect(Collectors.joining(", ", "{", "}")) +
                ", Furniture: " + furniture.stream().map(l -> l.toString()).collect(Collectors.joining(", ", "{", "}")) +
                ", Doors: " + doors.stream().map(l -> l.toString()).collect(Collectors.joining(", ", "{", "}")) +
                "}";
    }
}
