package omo.home.model.sportEquipment;

import omo.home.model.rooms.Room;

public class Bike extends SportEquipment{

    public Bike(Room room){
        super(room);
    }
}
