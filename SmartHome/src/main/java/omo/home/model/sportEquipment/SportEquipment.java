package omo.home.model.sportEquipment;

import omo.home.model.people.Person;
import omo.home.model.rooms.Room;

public abstract class SportEquipment {
    private boolean beingUsed = false;
    private Room room;
    private Person user;

    public void updateSportEqipment(int hour){
        if(beingUsed){
            System.out.println("Equipment " + this.getClass().getSimpleName() + " is being used by: " + user.getName());
        }
    }

    public void setUser(Person user){
        this.user = user;
        this.beingUsed = true;
    }

    public void setUser(){
        this.user = null;
        this.beingUsed = false;
    }

    public boolean getBeingUsed(){
        return beingUsed;
    }

    public SportEquipment(Room room){
        this.room = room;
    }

    public class SportEquipmentFactory{

        public SportEquipment createSportEquipment(String type, Room room){
            switch (type.toLowerCase()) {
                case "bike":
                    return new Bike(room);
                case "ski":
                    return new Ski(room);
                default:
                    throw new IllegalArgumentException("Unsupported appliance type: " + type);
            }
        }
    }

    public String toString(){
        return "Equipment: " + this.getClass().getSimpleName() + ", being used: " + beingUsed;
    }
}
