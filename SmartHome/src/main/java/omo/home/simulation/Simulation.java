package omo.home.simulation;

import omo.home.model.Home;

import java.util.logging.Level;

public class Simulation {
    public static void main(String[] args) {

        startSimulation(2);

    }

    /**
     *
     * @param hoursTotal hours the simulation will run for
     */
    public static void startSimulation(int hoursTotal){

        Home home = new Home.HomeFactory().createHome(2);

        // all - prints content of all floors and rooms on update, prints location and current activity of each person on update
        // off - doesn't do the above
        home.setLoggerLevel(Level.ALL);


        // Simulation loop
        for (int hour = 1; hour <= hoursTotal; hour++) {

            // Update the state of the home for the current hour
            home.updateState();

            // Simulate the passage of time by waiting
            simulateTimePassage();
        }

        // Simulation completed
        System.out.println("Simulation completed.");
    }

    /**
     * simulates time passage by making the thread sleep
     */
    private static void simulateTimePassage() {
        try {
            Thread.sleep(800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}